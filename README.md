# Filtering Techniques for Low-Noise Previews of Interactive Stochastic Ray Tracing
This is my [dissertation](diss_lq.pdf) on denoising techniques for interactive path tracing---published in 2013 and ignored ever since.

## Abstract
Progressive stochastic ray tracing is increasingly used in interactive applications. Examples of such applications are interactive design reviews and digital content creation. This dissertation aims at advancing this development. For one thing, two filtering techniques are presented, which can generate fast and reliable previews of global illumination solutions. For another thing, a system architecture is presented, which supports exchangeable rendering back-ends in distributed rendering systems.

## Why is this here?
The text is available at the [Eurographics Digital Library](https://diglib.eg.org/handle/10.2312/8318) and the [Universitäts- und Landesbibliothek Darmstadt](https://tuprints.ulb.tu-darmstadt.de/3590/), but over the years a few people have asked me for the original images and videos.
Now there is a [GitLab repository](https://gitlab.com/kschwenk/diss) and a [GitLab Pages page](https://kschwenk.gitlab.io/diss) where these things can be downloaded from.
The file [`diss.pdf`](diss.pdf) is the dissertation with glorious high-quality images.
The file [`diss_lq.pdf`](diss_lq.pdf) is a downsampled version.
The file [`lossless_videos.zip`](https://gitlab.com/kschwenk/diss/-/blob/master/lossless_videos.zip) contains some captured videos in lossless encoding.
Most video codecs do not handle noise well, and obviously the noise patterns are quite important here.
The file [`supplemental.zip`](https://gitlab.com/kschwenk/diss/-/blob/master/supplemental.zip) contains the original images and videos (in lossy encoding) for the papers that went into Chapters 3 and 4 of the dissertation.
It also contains the original technical reports.
The files [`techrep_ch3.pdf`](techrep_ch3.pdf) and [`techrep_ch4.pdf`](techrep_ch4.pdf) are these technical reports, extracted for convenience and direct linking.
The file [`index.html`](index.html) is an html version of [`README.md`](README.md) for direct web access via the [GitLab Pages page](https://kschwenk.gitlab.io/diss).

## Looking back ten years later
Looking back, the techniques developed here had some nice new ideas in them, and they performed very well for the time, if I say so myself.
Although back then this whole denoising for interactive path tracing thing was a bit of a niche subject.
(Most of the work was done from 2010 to 2013.)
Overall however, the techniques are not directly relevant anymore.
The Earth has revolved a few times more around the Sun, graphics hardware has done its thing, and what was barely possible interactively (around 1 second rendering time) is now becoming possible in real-time (around 30 milliseconds rendering time)---and the real-time world has different trade-offs to make.
Still, some of the ideas survived in related methods, so I like to think I did not completely waste my time.
I am particularly pleased with the two technical reports that turned into chapters 3 and 4 of the [dissertation](diss_lq.pdf).

The first report, [Filtering and Blending of High-Variance Light Paths with Perceptual Control](techrep_ch3.pdf), introduced the concept of a "range buffer" to guide an edge-preserving filter, a technique widely used in subsequent work and popularized under the term "virtual flash image".
This work was largely done throughout 2010 and early 2011.
A shortened version of the report was submitted to Computer Graphics and Applications in August 2011.
Unfortunately, the paper was badly mauled in the review process, at least in my opinion, and what eventually [appeared in print](https://ieeexplore.ieee.org/document/6127845) in December 2012 had the description of the filter almost completely removed and pushed into the supplementary material.
(Pro tip for young researchers: If a reviewer desperately tries to drag out the review process and to push contributions out of the paper, don't give in to their demands---you're getting fucked over.)
Anyway, if you are interested in that technique and how it came about read the technical report, not the paper.

The second report, [Radiance Filtering: Interactive, Low-Noise Previews of Path Traced Global Illumination](techrep_ch4.pdf), introduced a technique to reuse samples of incident radiance from neighboring pixels.
I called this "radiance filtering", although in retrospect I find that name misleading, because it conjures up an image where the radiance is blurred and then put through the brdf, which is not what happens.
Instead individual samples of incident radiance are put through the brdf, the samples of incident radiance are taken from neighboring pixels, but the brdf is taken from the pixel currently being estimated.
This massively reduces noise with relatively little blurring.
(Putting individual radiance samples through the brdf is better than "pre-blurring" radiance and putting that through the brdf, and features of the materials like color textures and normal maps are not blurred at all because only the material properties of the current pixel enter the estimate.)
The technique was presented as a [poster](https://dl.acm.org/doi/abs/10.1145/2342896.2343022) at SIGGRAPH 2012.
I tried to get a paper published after that, but it didn't make it through peer review in 2012, and because publication of the dissertation was on the horizon in 2013 I didn't pursue the paper further.
In my completely biased opinion this technique had an extremely good balance of image quality and speed for the time.
Although the rest of the world apparently didn't share this opinion---or did and found it too easy to ignore the poster.
Anyway, I always intended this as a stepping stone to filtering techniques that would work on the space of light paths instead of on the space of pixels, but unfortunately I never got around to doing that.

## License
[CC BY-NC-ND 2.5](https://creativecommons.org/licenses/by-nc-nd/2.5/)
